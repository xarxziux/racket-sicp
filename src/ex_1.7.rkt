#lang racket
(define (sqrt-iter old-guess guess x)
  (if (good-enough? old-guess guess)
      guess
      (sqrt-iter
       guess
       (improve guess x)
       x)))

(define (improve guess x)
  (average guess (/ x guess)))

(define (good-enough? old-guess guess)
  (< (abs (- 1 (/ old-guess guess))) 0.00000001))

(define (average x y)
  (/ (+ x y) 2))

(define (square x) (* x x))

(define (sqrt-rec x)
  (sqrt-iter x 1.0 x))

(square (sqrt-rec 19))

(square (sqrt-rec 15))

(square (sqrt-rec 105))

(square (sqrt-rec 200))
